const express = require('express')
const jwt = require('jsonwebtoken');
const user = require('./Modals/user');
const { JWT_TOKEN } = require('./Config/Key');


module.exports=(req,res,next)=>{
    const {authorization} = req.headers
    if(!authorization){
        res.status(404).send('you have to logged in')
    }
    const token = authorization.replace("Bearer ","")
    jwt.verify(token,JWT_TOKEN,(err,payload)=>{
        if(err){
            console.log(err)
            res.status(404).send('you have to logged in for a while')
        }
        const{_id} = payload
        user.findById(_id)
        .then((userdata)=>{
            req.user = userdata
            next()
        })
    })
}