const express = require('express');
const router = express.Router()
const mongoose = require('mongoose')
const vet  = require('../Modals/vet')

router.post('/addvet',(req,res)=>{
    const { name, email, Address,state,district,contact,Timing,pic} = req.body
    const Vet = new vet({
        name,
        email,
        Address,
        contact,
        Timing,
        pic,
        state,
        district
    })
    Vet.save()
    .then((Vet)=>{
        res.status(422).json(Vet)
    })
    .catch((err)=>console.log(err))
})

router.get('/getvet',(req,res)=>{
    vet.find({})
    .then((Vet)=>{
     res.status(422).json(Vet)
    }).catch(err=>console.log(err))
    
})

module.exports = router;
