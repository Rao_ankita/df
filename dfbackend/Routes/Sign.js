const express = require('express');
const router = express.Router()
const mongoose = require('mongoose')
const user = require('../Modals/user')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { JWT_TOKEN } = require('../Config/Key')
const requireLogin = require('../middleware')

router.post('/signup', (req, res) => {
    const { name, email, password, pic, contact, address, ratings, description } = req.body
    if (!name || !email || !password || !contact || !address || !ratings || !description||!state||!district) {
        return res.status(422).json({ error: "please fill all the details" })
    }
    user.findOne({ email: email }).then((saveduser) => {
        if (saveduser) {
            return res.status(422).json({ error: "email exists" })
        }
        bcrypt.hash(password, 12)
            .then(hashedpassword => {
                const User = new user({
                    name,
                    email,
                    password: hashedpassword,
                    pic,
                    contact,
                    description,
                    ratings,
                    address,
                    state,
                    district
                })
                User.save()
                    .then(User => {
                        res.json({ message: "data saved" })
                    })
                    .catch(err => {
                        console.log(err)
                    })
            })

    })

        .catch(err => {
            console.log(err)
        })
});


router.post('/signin', (req, res) => {
console.log(req.body)
    const { email, password } = req.body
    if (!email || !password) {
        res.status(422).json({ message: 'please fill all details' })
    }
    user.findOne({ email, email })
        .then(saveduser => {
            if (!saveduser) {
                res.status(422).json({ message: 'invalid email or password' })
            }
            bcrypt.compare(password, saveduser.password)
                .then(domatch => {
                    if (domatch) {
                        //    res.status(422).json({message:'successfully signed in'}) 
                        const token = jwt.sign({ _id: saveduser._id }, JWT_TOKEN)
                        console.log("token is:", token)
                        res.json({token,saveduser})
                    } else {
                        res.status(422).json({ message: 'invalid email or password' })
                    }
                })
                .catch(error => {
                    console.log(error)
                })
        })
});

router.get('/alldairyfarmowner', (req, res) => {
    user.find({})
        .then((User) => {
            res.status(422).json(User)
        }).catch(err => console.log(err))
});

router.get('/user',requireLogin,(req,res)=>{
    user.findById({_id:req.user._id})
    .then((User) => {
        res.status(422).json(User)
    }).catch(err => console.log(err))
})

router.put('/ratingupdate',requireLogin,(req,res)=>{
    user.findByIdAndUpdate({_id:req.user._id},
        {ratings:req.body.ratings},
        {new:true},(err,result)=>{
            if(err){
              console.log(err)
            }
            res.status(422).json(result)
        })
    
})

module.exports = router;