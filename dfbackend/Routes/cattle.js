const express = require('express');
const router = express.Router()
const mongoose = require('mongoose')
const cattle = require('../Modals/cattle')
const requireLogin = require('../middleware')

router.post('/cattle',requireLogin,(req,res)=>{
   console.log(req.body.fodder)
    const {breed,pregnant,pregmonths,
      recentdelvmonth,disease,lastdeliveries,pic,temperature,
      description,milkprod,fat,sellrate,marketrate} = req.body
     const Cattle = new cattle({
        breed,
        pregnant,
        pregmonths,
        recentdelvmonth,
        disease,
        lastdeliveries,
        pic,
        temperature,
        description,
        milkprod,
        fat,
        sellrate,
        marketrate,
        postedBy:req.user
     })
     Cattle.save()
     .then((Ccattle)=>{
        res.status(422).json({message:'cattle field saved successfully'})
     }).catch(err=>console.log(err))
})

router.put('/updatecattle',requireLogin,(req,res)=>{
   console.log(req.body)
    cattle.findByIdAndUpdate({_id:req.body.id},{
        pregnant:req.body.pregnant,
        pregmonths:req.body.pregmonths,
        recentdelvmonth:req.body.recentdelvmonth,
        disease:req.body.disease,
        lastdeliveries:req.body.lastdeliveries,
        temperature:req.body.temperature,
        milkprod:req.body.milkprod,
        fat:req.body.fat,
        sellrate:req.body.sellrate,
        marketrate:req.body.marketrate
    },
        {new:true}).then((Catlle)=>{
           res.status(422).json({message:'data updated'})
        }).catch((err)=>{console.log(err)})
})   

router.put('/fodder',requireLogin,(req,res)=>{
   console.log(req.body)
   cattle.findByIdAndUpdate({_id:req.body.id},{$push:{fodder:req.body.fodder}},{new:true})
   .then((result)=>{
        res.status(422).json({message:'fodder added'})
   }).catch(err=>console.log(err)) 
});

router.put('/updatefodder',requireLogin,(req,res)=>{
     
})

router.get('/allcattle',(req,res)=>{
   cattle.find({})
   .then((cattle)=>{
      res.status(422).json(cattle)
   }).catch(()=>console.log(err))
})

module.exports = router;