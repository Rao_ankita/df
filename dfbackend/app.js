const express = require('express');
const app = express();
const PORT = 8000;

const mongoose = require('mongoose')
const cron = require('node-cron')

const db = require('./Config/Key').MongoURI

mongoose.connect(db,{useNewUrlParser:true,useUnifiedTopology: true})
.then(()=>console.log('mongodb connected'))
.catch(err=>console.log(err))

app.use(express.json())

require('./Modals/user')
require('./Modals/cattle')
require('./Modals/vet')

app.use(require('./Routes/Sign'))
app.use(require('./Routes/cattle'))
app.use(require('./Routes/vet'))

cron.schedule('* * * * *', () => {
    console.log('running a task every minute');
  });


  cron.schedule('* * * * *', () => {
    console.log('hey!!!!!!i am another cron scheduler');
  });

app.listen(PORT,()=>{console.log(`server is listen at ${PORT}`)});