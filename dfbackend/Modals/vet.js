const mongoose = require('mongoose');

const VetSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    Address:{
        type:String,
        required:true
    },
    state:{
        type:String,
        required:true
    },
    district:{
        type:String,
        required:true
    },
    contact:{
        type:Number,
        required:true
    },
    Timing:{
        type:String,
        required:true
    },
    pic:{
        type:String,
        default:'https://res.cloudinary.com/ankita1297/image/upload/v1590556247/defaultpic_znezyw.png'
    },
},{
    timestamps:true
})

module.exports = mongoose.model('vet',VetSchema)