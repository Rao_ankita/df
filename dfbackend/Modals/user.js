const express = require('express');
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    contact:{
        type:Number,
        required:true
    },
    pic:{
        type:String,
        default:'https://res.cloudinary.com/ankita1297/image/upload/v1590556247/defaultpic_znezyw.png'
    },
    address:{
        type:String
    },
    state:{
        type:String
    },
    district:{
        type:String
    },
    ratings:{
        type:Number,
        default:1
    },
    Description:{
        type:String
    },
    photo:[
        {
            type:String,
            default:'https://res.cloudinary.com/ankita1297/image/upload/v1590556247/defaultpic_znezyw.png'
        }
    ]

},{
    timestamps:true
})

module.exports = mongoose.model('user',userSchema);