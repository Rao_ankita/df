import React,{ useState}from 'react';
import { Link } from 'react-router-dom';
import { useEffect } from 'react';
function SignUp() {
  const [name,setName] = useState("");
  const [email,setEmail] = useState("");
  const [password,setPassword] = useState("");
  const [contact,setContact] = useState("");
  const [address,setAddress] = useState("");
  const [state,setState] = useState("");
  const [district,setDistrict] = useState("");
  const [image,setImage] = useState("");
  const [pic,setPic] = useState(undefined);
   const [description,setDescription] = useState("");
   useEffect(()=>{
     if(pic){
      uploadDetails();
     }
   },[pic]);
    const postData = ()=>{
      if(image){
         uploadDetails();
      }
      else{
        uploadPic();
      }
    }
  const uploadPic  = ()=>{
    const data = new FormData()
    data.append("file", image)
    data.append("upload_preset", "inst-clone")
    data.append("cloud_name", "ankita1297")
    fetch("https://api.cloudinary.com/v1_1/ankita1297/image/upload", {
      method: "post",
      body: data
    })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setPic(data.url)
      })
      .catch(err => {
        console.log(err)
      })
  }

  const uploadDetails = ()=>{
    fetch('http://localhost:8000/signin',{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name:name,
        email:email,
        password:password,
        contact:contact,
        address:address,
        ratings:4,
        description:description,
        state:state,
        district:district,
        pic:pic,
      })
    })
  }

  return (
    <div className="mycard">
      <div className="card auth-card input-field">
        <h2 style={{ marginTop: 30 }}>Sign Up</h2>
        <input type="text" placeholder="Name" style={{ marginTop: 20 }} onChange={(e)=>setName(e.target.value)} />
        <input type="text" placeholder="Email" style={{ marginTop: 20 }} onChange={(e)=>setEmail(e.target.value)} />
        <input type="password" placeholder="Password" style={{ marginTop: 10 }} onChange={(e)=>setPassword(e.target.value)} />
        <input type="text" placeholder="Contact" style={{ marginTop: 10 }} onChange={(e)=>setContact(e.target.value)} />
        <input type="text" placeholder="Address" style={{ marginTop: 10 }} onChange={(e)=>setAddress(e.target.value)} />
        <div>
          <select className="browser-default" onChange={(e)=>setState(e.target.value)}>
            <option value="0" default value>Choose your State</option>
            <option value="Haryana">Haryana</option>
            <option value="Delhi">Delhi</option>
            <option value="Punjab">Punjab</option>
          </select>
          <div />
          <select className="browser-default" onChange={(e)=>setDistrict(e.target.value)}>
            <option value="0" default value>Choose your District</option>
            <option value="gurgaon">gurgaon</option>
            <option value="Faridabad">Faridabad</option>
            <option value="Sonipat">Sonipat</option>
          </select>
          <input type="text" placeholder="Description" style={{ marginTop: 10 }} onChange={(e)=>setDescription(e.target.value)}/>
          <div className="file-field input-field">
                <div className="btn #6a1b9a purple darken-3">
                    <span>Profile Photo</span>
                    <input type="file" onChange={(e) => setImage(e.target.files[0])}/>
                </div>
                <div className="file-path-wrapper">
                    <input className="file-path validate" type="text" />
                </div>


            </div>
          <button className="waves-effect waves-light btn #6a1b9a purple darken-3"
            style={{ marginTop: 10, textColor: "#fafafa grey lighten-5" }} onClick={()=>postData()}>Signup</button>
          <h5 style={{ marginTop: 10 }}><Link to="/signup">Have an account?</Link></h5>
        </div>
      </div>
</div>
    );
   
}
export default SignUp;