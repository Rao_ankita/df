import React,{ useState} from 'react';
import {Link} from 'react-router-dom'
function Login() {
  const [ email,setEmail] = useState('');
  const [password,setPassword] = useState('');
  // console.log(name);
  // console.log(password);
const Submit = ()=>{
  fetch('http://localhost:8000/signin',{
    method:'post',
    headers: {
      'Content-Type':'application/json'
    },
    body: JSON.stringify({
      email:email,
      password:password
    })
  } 
  ).then((res)=>res.json())
  .then((result)=>{
    console.log(result);
    localStorage.setItem('token',result.token);
    localStorage.setItem('user',result.user);
  })
  .catch((err)=>{
    console.log(err);
  })
}
  return (
    <div className="mycard">
      <div className="card auth-card input-field">
        <h2 style={{marginTop:30}}>Login</h2>
        <input type="text" placeholder="Email" style={{marginTop:20}} onChange={(e)=>setEmail(e.target.value)}/>
        <input type="password" placeholder="Password" style={{marginTop:10}} onChange={(e)=>setPassword(e.target.value)}/>
        <button className="waves-effect waves-light btn #6a1b9a purple darken-3"
        onClick = {()=>Submit()}
        style={{marginTop:10,marginLeft:90,textColor:"#fafafa grey lighten-5",width:150}}>Login</button>
        <h5 style={{marginTop:10}}><Link to="/Signin">Don't have an account?</Link></h5>
      </div>
    </div>

    );
   
}
export default Login;