import React from 'react';
import {BrowserRouter,Route,Switch} from 'react-router-dom'
import BuySell from'./Components/screen1'
import SignUp from './Components/Signup'
import Login from './Components/Login'
//import Navbar from './Components//navbar.js';
import Sidebar from './Components/Sidebar'
// import Footer from './Components/Footer';
function App() {
  return (
    <>
   
    <Switch>
    <Route exact path = "/"
    component = {Login}   />
    <Route path = "/Signin"
    component = {SignUp} />   
    <Route path = "/Home"
    component = {Sidebar} />   
    </Switch>
    
     
    </>
  );
}

export default ()=>{
  return(
    <BrowserRouter>
    <App />
    </BrowserRouter>
  )
};
